package main

import (
	"github.com/ant0ine/go-json-rest/rest"
	"github.com/jackc/pgx"
	"log"
	"net/http"
)

type Api struct {
	DB *pgx.Conn
}

type AssetCode struct {
	Asset_type_code string
}

type Asset struct {
	Asset_type_id int32
	Asset_type_name	string
	Asset_type_description	string
	Mobility_type_description	string
}

type Site struct {
	Mining_site_id	int32
	Site_name	string
	Site_code	string
}

func extractConfig() pgx.ConnConfig {
	var config pgx.ConnConfig

	config.Host = "masterdata-asset20180327011614905900000004.cluster-colpkcgm5tyt.us-west-2.rds.amazonaws.com"
    config.User = "bdfreader"
    config.Password = "Bdf1Reader2?!*"
    config.Database = "abx_masterdata"

	return config
}

func (apiConfig *Api) InitDB() {
	var err error
	apiConfig.DB, err = pgx.Connect(extractConfig())
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
}

func main() {
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	apiConfig := Api{}
	apiConfig.InitDB()

	router, err := rest.MakeRouter(
		rest.Get("/getassets/:assetcode", apiConfig.GetAssets),
		rest.Get("/getassetcodes", apiConfig.GetAssetCodes),
		rest.Get("/sites", apiConfig.GetSites),
	)

	if err != nil {
		log.Fatal(err)
	}
	
	api.SetApp(router)

	log.Fatal(http.ListenAndServe(":8081", api.MakeHandler()))
}

func setupResponse(w *rest.ResponseWriter, r *rest.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
    (*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func (apiConfig *Api) GetSites (w rest.ResponseWriter, r *rest.Request) {
	sites := make([]Site, 0)

	rows, err := apiConfig.DB.Query("SELECT mining_site_id, site_name, site_code from conformed_view.mining_site")
	
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {	
		var mining_site_id	int32
		var site_name	string
		var site_code	string

		if err := rows.Scan(&mining_site_id, &site_name, &site_code); err != nil {
			log.Fatal(err)
		}
		sites = append(sites, Site{Mining_site_id: mining_site_id, Site_name: site_name, Site_code: site_code})
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	setupResponse(&w, r)
	w.WriteJson(&sites)

}


func (apiConfig *Api) GetAssetCodes(w rest.ResponseWriter, r *rest.Request) {
	codes := make([]AssetCode, 0)

	rows, err := apiConfig.DB.Query("SELECT asset_type_code from conformed_view.asset_type")
	
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var asset_type_code string

		if err := rows.Scan(&asset_type_code); err != nil {
			log.Fatal(err)
		}
		codes = append(codes, AssetCode{Asset_type_code: asset_type_code})
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	setupResponse(&w, r)
	w.WriteJson(&codes)

}


func (apiConfig *Api) GetAssets(w rest.ResponseWriter, r *rest.Request) {

	assetCode := r.PathParam("assetcode")
	assets := make([]Asset, 0)

	rows, err := apiConfig.DB.Query("SELECT asset_type_id, asset_type_name, asset_type_description, mobility_type_description from conformed_view.asset_type where asset_type_code = $1", assetCode)
	
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var asset_type_id int32
		var asset_type_name	string
	    var asset_type_description	string
		var mobility_type_description	string

		if err := rows.Scan(&asset_type_id, &asset_type_name, &asset_type_description, &mobility_type_description); err != nil {
			log.Fatal(err)
		}
		assets = append(assets, Asset{Asset_type_id: asset_type_id, Asset_type_name: asset_type_name, Asset_type_description: asset_type_description, Mobility_type_description: mobility_type_description})
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	setupResponse(&w, r)

	w.WriteJson(&assets)
}

// curl -i http://127.0.0.1:8080/getassets/DOZ